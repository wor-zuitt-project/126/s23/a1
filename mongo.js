
//1 

db.listingsAndReviews.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
})



//2


db.listingsAndReviews.insertMany([
		{
			"name": "double",
			"accomodates": 3,
			"price": 2000,
			"description": "A room fit for a small family going on a vacation",
			"rooms_available": 5,
			"isAvailable": false
		},
		{
			"name": "queen",
			"accomodates": 4,
			"price": 4000,
			"description": "A room with a queen sized bed perfect for a simple getaway",
			"rooms_available": 15,
			"isAvailable": false
		}

])


//3 

db.listingsAndReviews.find(
		{
			"price": {
				$gte: 2000
			}
		})

//4 

db.listingsAndReviews.find(
		{
			"price": {
				$gte: 2000
			},
			"rooms_available": {
				$gt: 10
			}
		}).limit(1)

//5 
db.listingsAndReviews.updateOne(
	{
		"name": "queen"
	},
	{
	$set: { "rooms_available": 0}
	}
)


//6 

db.listingsAndReviews.updateMany(
		{
			"rooms_available": {
				$gt: 0
			},
			"isAvailable": false
		},
		{
		 $set: { "isAvailable": true}
		}
	)


//7

db.listingsAndReviews.deleteMany({
	"rooms_available": 0
})


//8 

db.listingsAndReviews.find(
	{
		"name": "double"
	},
	{
		"name": 1,
		"accomodates": 1,
		"price": 1,
		"description": 1
	}
)


//9


db.listingsAndReviews.find(
	{
		"name": "double"
	},
	{
		"name": 1,
		"accomodates": 1,
		"price": 1,
		"description": 1,
		"_id": 0
	}
)